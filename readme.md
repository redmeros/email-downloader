Email downloader - should be simple pop3 client to download emails
from local email server and put content to certain destination
based on the subject of the email.

The email should be sent with certain subject.

The format is:

`@xxx [yyyyy]`

Where xxx - is a project number and #yyyy is description (created via ED)

For example expected behaviour after receiving such email is:
to: `arch@dcssc.local`
subject: `@602 [test subject]`
content: `test content`
attachments: `testattachment.txt`

The script should find directory in /srv/..../ that match some regular expression
then go to the `602-KOR` and there create new folder `YYYY-MM-DD - [yyyy]`
Then put inside attachment and content in content.txt file.

included html2txt.py (all credits to http://www.aaronsw.com/2002/html2text/)