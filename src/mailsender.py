import pyzmail

class MailSender:
    """
    Class that supports sending back mail to sender if sth goes wrong.
    The class should send email with error, and with the email as attachment.

    The email should contain also some info how to repair the error.
    """
    message = ""
    to = ""
    attachemnt = None

    def __init__(self):
        pass

    def add_attachment(self, attachment):
        self.attachemnt = attachment

    def send(self):
        prefered_encoding = "utf-8"
        text_encoding = "utf-8"
        text_content = self.message
        subject = u"Błąd podczas przetwarzania wiadomości"
        sender = (u"ZBot", 'arch@dcssc.local')
        recipients = [self.to,]
        att = self.attachemnt.as_string()
        # att = 
        payload, mail_from, rcpt_to, msg_id = pyzmail.compose_mail(\
        sender,\
        recipients,\
        subject,\
        prefered_encoding,\
        (text_content, text_encoding),\
        html=None,
        attachments=[(att, 'application', 'message', 'message.eml', 'utf-8'),]
        )

        smtp_host='mail.dcssc.local'
        smtp_port='2500'
        smtp_mode=None

        ret = pyzmail.send_mail(payload, mail_from, rcpt_to, smtp_host, smtp_port, smtp_mode='normal')
        if isinstance(ret, dict):
            if ret:
                print ('failed recipients:', ', '.join(ret.keys()))
            else:
                print ('success')
        else:
            print ('error: ', ret)