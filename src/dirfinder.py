import glob
import re
import os
from Exceptions import EmailDownloaderException

class DirFinder:
    """Class that can find proper directory"""
    init_dir = ""

    def __init__(self, initial_dir = ""):
        self.init_dir = initial_dir

    def find(self, ProjectNo):
        depth = glob.glob(self.init_dir + "/*/*")
        dirs = filter(lambda f: os.path.isdir(f), depth)

        rgx = re.compile("^" + str(ProjectNo) + "-")
        for d in dirs:
            if not os.path.isdir(d):
                continue
            if len(rgx.findall(os.path.basename(d))) != 1:
                continue
            return os.path.abspath(d)
        raise EmailDownloaderException("Nie znalazłem katalogu z projektem init_dir={0}, project_no={1}".format(self.init_dir, str(ProjectNo)))