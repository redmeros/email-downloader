"""
Module that declares email-downloader exceptions
"""

class EmailDownloaderException(Exception):
    """Main exception class for easy filtering"""
    def __init__(self, message):
        super().__init__(message)
