import re
from Exceptions import EmailDownloaderException

class SubjectParser:
    """Parses the subject according to regex in settings"""

    ProjectNo = -1
    Title = ""
    context = ""
    dirname_regex = ""
    project_no_regex = ""

    def __init__(self, subject, dirname_regex="", project_no_regex=""):
        self.dirname_regex = dirname_regex
        self.project_no_regex = project_no_regex

        self.context = subject

        if dirname_regex and project_no_regex:
            self.parse(subject)

    def parse(self, subject=""):
        """Parses current subject against project no and project title"""

        if not subject:
            subject = self.context
        if not subject:
            raise EmailDownloaderException("Próba parsowania pustego tematu")

        self.parse_project_no(subject)
        self.parse_project_title(subject)
    
    def parse_project_title(self, subject):
        """Parses current subject against project title"""
        rgx = re.compile(self.dirname_regex)
        res = rgx.findall(subject)
        if len(res) != 1:
            raise EmailDownloaderException("Blad podczas parsowania tematu wiadomosci (nierozpoznano katalogu do zalozenia) - prawdopodobnie blad w skladni")
        self.Title = res[0].replace(":", "-")

    def parse_project_no(self, subject):
        """Parses project number"""
        rgx = re.compile(self.project_no_regex)
        res = rgx.findall(subject)
        if len(res) != 1:
            raise EmailDownloaderException("Blad podczas parsowania tematu wiadomosci (nie rozpoznano numeru tematu) - prawdopodobnie blad w skladni")
        self.ProjectNo = int(res[0])