"""
For description please look at readme.md
"""

import json
import os
import poplib

from datetime import datetime

import time
from pyzmail import PyzMessage

import html2text
import schedule

from dirfinder import DirFinder
from subjectparser import SubjectParser
from Exceptions import EmailDownloaderException
from mailsender import MailSender

SETTINGS_FILE = "settings.json"
SETTINGS_DATA = {}

# to jest brzydkie jak cholera - ustawienia powinny 
# byc jakos inaczej ogarniane moze gdzies w init ??
def read_settings():
    """Reads settings from the settings.json file, which is located one folder up"""

    global SETTINGS_FILE
    global SETTINGS_DATA
    
    with open(SETTINGS_FILE, mode="rt") as json_data:
        SETTINGS_DATA = json.load(json_data)


class Downloader:
    """Message downlader"""
    SETTINGS_DATA = None

    read_messages = []

    """Constructor"""
    def __init__(self):
        # read_settings()
        pass

    def save_message(self, message, parent_directory, subject):
        """save message to the directory. It creates new dir in the format YYYY-MM-DD - SUBJECT"""
        today = datetime.today()
        new_dir = os.path.join(parent_directory, today.strftime("%Y-%m-%d") + " - " + subject)
        print("Tworzę nowy katalog: ", new_dir)
        try:
            os.mkdir(new_dir)
        except FileExistsError:
            print("Folder juz istnieje nie tworze nowego")
        except OSError:
            print("Blad podczas tworzenia katalogu")
        if not os.path.isdir(new_dir):
            print("Błąd katalog nie został stworzony")
            raise EmailDownloaderException("Nie mogę stworzyć katalogu: " + new_dir)
        # Od tad jest prawdziwy zapis
        
        for part in message.mailparts:
            #print(part.is_body)
            if part.is_body == 'text/plain':
                with open(os.path.join(new_dir, "WIADOMOSC.txt"), "wt") as f:
                    f.write(part.get_payload().decode(part.charset))
            elif part.is_body == 'text/html':
                with open(os.path.join(new_dir, "WIADOMOSC.txt"), "wt", newline='\r\n') as f:
                    txt = part.get_payload().decode(part.charset)
                    txt = html2text.html2text(txt)
                    f.write(txt)
                with open(os.path.join(new_dir, "WIADOMOSC.html"), "wt", newline='\r\n') as f:
                    f.write(part.get_payload().decode(part.charset))
            else:
                #print(part.filename)
                filename = part.filename
                if filename is None:
                    filename = "brak_nazwy"
                with open(os.path.join(new_dir, filename), "wb") as f:
                    f.write(part.get_payload())
        return True


    def del_read_messages(self):
        """Deletes messages that had been read from server using pop3"""
        if not self.read_messages:
            return
        pop = poplib.POP3(SETTINGS_DATA['host'])
        try:
            pop.user(SETTINGS_DATA['login'])
            pop.pass_(SETTINGS_DATA['pass'])
            print("usuwam...")
            while self.read_messages:
                msgno = self.read_messages.pop()
                print("usuwam", msgno)
                print(pop.dele(msgno))
        finally:
            pop.quit()

    def get_new_messages(self):
        """Downloading all of the messages from the server"""
        pop = poplib.POP3(SETTINGS_DATA['host'])
        mails = []

        try:
            pop.user(SETTINGS_DATA['login'])
            pop.pass_(SETTINGS_DATA['pass'])
            msginfo = pop.list()[1]
            for info in msginfo:
                info = info.decode("utf-8")
                msg_number = int(info.split(" ")[0])
                msg = pop.retr(msg_number)[1]
                msgstr = ""
                for row in msg:
                    msgstr += row.decode("latin-1")
                    msgstr += "\n"

                msg = PyzMessage.factory(msgstr)
                self.read_messages.append(msg_number)
                mails.append(msg)
            return mails


        except poplib.error_proto as ex:
            print(ex)
        finally:
            print(pop.quit())


def job():
    read_settings()
    mydownloader = Downloader()
    print("Downloading messages")

    mails = mydownloader.get_new_messages()
    print( "{0} mails read.".format(len(mails)))

    for mail in mails:
        try:
            print("Czytam info z tematu...")
            project_info = SubjectParser(mail.get_subject())
            project_info.dirname_regex = SETTINGS_DATA['dirname_regex']
            project_info.project_no_regex = SETTINGS_DATA['project_no_regex']
            project_info.parse()
            print("Odnalazlem oznaczenie tematu: ", project_info.ProjectNo)
            print("Info odczytane z tematu: ", project_info.Title)
            print("Rozpoczynam szukanie katalogu, zaczynam od", SETTINGS_DATA['localpath'])
            finder = DirFinder(SETTINGS_DATA["localpath"])
            path = finder.find(project_info.ProjectNo)
            print ("Znalazlem nastepujacy katalog z projektem:", path)
            kor_dir = os.path.join(path, str(project_info.ProjectNo) + "-" + "KOR")
            print ("Katalog koordynacyjny jest nastepujacy: " + kor_dir)
            if not os.path.exists(kor_dir):
                print("Katalog koordynacyjny nie istnieje - SPROBUJE GO ZALOZYC")
                os.makedirs(kor_dir)
                if not os.path.exists(kor_dir):
                    raise EmailDownloaderException("Blad podczas zakladania katalogu koordynacyjnego: " + kor_dir)
                print("Udalo sie zalozyc katalog - jedziemy dalej")
            print("Następuje próba zapisu wiadomości w katalogu z koordynacyjnym")
            mydownloader.save_message(mail, kor_dir, project_info.Title)
        except EmailDownloaderException as ex:
            print(ex)
            m = MailSender()
            m.add_attachment(mail)
            m.message = str(ex)
            m.to = mail.get_address("from")
            m.send()
        finally:
            if not DEBUG:
                print ("Nastąpi próba usunięcia wiadomości")
                mydownloader.del_read_messages()


DEBUG = False

if __name__ == "__main__":
    if DEBUG is True:
        job()
        exit()

    schedule.every(10).seconds.do(job)
    
    while True:
        schedule.run_pending()
        time.sleep(10)
    

    
